package cn.mcp.chat

import cn.mcp.core.MCPlugin
import cn.mcp.core.buildAllChat
import cn.mcp.core.ktx.registerEvents
import org.bukkit.World
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent

class ChatPlugin : MCPlugin(), Listener {


    override fun onEnable() {
        super.onEnable()
        registerEvents(this)
    }


    @EventHandler
    fun onAsyncPlayerChat(event: AsyncPlayerChatEvent) {
        val player = event.player
        val world = player.world
        val prefix = when (world.environment) {
            World.Environment.NORMAL -> {
                "主世界"
            }
            World.Environment.NETHER -> {
                "下界"
            }
            else -> {
                "末地"
            }
        }
        event.format = buildAllChat("[$prefix]<Lv.${player.level}>%s说: %s")
    }
}